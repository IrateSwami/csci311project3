#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

// START Stuff that needs to be handled before main ----------------------------------------
// max file name length
#define MAX_FILE_NAME_SIZE 20

// date is typically mm/dd/yy, which is 8 characters
#define DATE_LENGTH 9

// length of the buffer
#define BUFFER_LENGTH 400

// the number of records we're willing to deal with at any time
#define MAX_AMOUNT_RECORDS 100

// buffer array to send things back and forth 
char BUFFER[BUFFER_LENGTH+1];

// struct for the "records", or single line of data read from our file
typedef struct record_t {

    int account_number;
    int check_number;
    float amount;
    char date[DATE_LENGTH];

} Record;
// END Stuff that needs to be handled before main ------------------------------------------

// BEGIN main ------------------------------------------------------------------------------
int main(int args, char *argv[]){

    Record records[MAX_AMOUNT_RECORDS];
    FILE *data;
    char file_name[MAX_FILE_NAME_SIZE];
    int i = 0;
 //   int read_pipe = atoi(argv[1]);
 //   int write_pipe = atoi(argv[2]);
    int pid = (int)getpid();


    // BEGIN initial file processing +++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // get the name of the file from the user
    printf("What file do you want to use? : ");
    scanf("%s", file_name);

    // get some variable ready to store those records
    int account_number;
    int check_number;
    float amount;
    char date[DATE_LENGTH];

    int count = 0;

    // open the file and associate a variable with it
    data = fopen(file_name, "r");

    // process all the records in the file and place them in the records array
    while((fscanf(data, "%d %d %s %f", 
    &account_number, 
    &check_number, 
    date, 
    &amount)) != EOF){

        records[count].account_number = account_number;
        records[count].check_number = check_number;
        records[count].amount = amount;

        // copy the string for date into the date array
        strcpy(records[count].date, date);
        
        // place a null character at the end of the date array
        records[count].date[9] = '\0'; 
        
        count++;
    }

    // close the file
    fclose(data);
    // END initial file processing +++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    /*
    // DEBUG print the size of the array
    printf("count = %d\n", count);      
    */

    /*
    // DEBUG print contents
    for (int i = 0; i < 12; i++){
        printf("%6s\n", records[i].date);
    }
    */



}
// END main --------------------------------------------------------------------------------
